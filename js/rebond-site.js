$(function () {
    R.init.base();
    Own.init.base();
    var $jsLauncher = $('.js-launcher');
    if ($jsLauncher.length) {
        $jsLauncher.each(function () {
            if (typeof R.init[$(this).val()] === 'function') {
                R.init[$(this).val()]();
            }
            if (typeof Own.init[$(this).val()] === 'function') {
                Own.init[$(this).val()]();
            }
        });
    }
});

(function (R) {
    R.init = {
        base: function () {
            $('#rb-nav').find('.has-sub-menu').hover(function () {
                if ($('#rb-burger').css('display') === 'block') {
                    return;
                }

                var $$ = $(this).children('.sub-nav');
                if ($$.length !== 0) {
                    $($$).toggleClass('sub-nav-hide');
                    // set x to sub menus
                    if ($(this).parent().hasClass('sub-nav')) {
                        $($$).css({left: $(this).width()});
                    }
                }
            });

            $('#rb-burger').click(function (e) {
                e.preventDefault();
                $('#rb-nav').children('.nav').toggleClass('mobile-show');
            });

            // back button
            $('.go-back').click(function (e) {
                e.preventDefault();
                if (document.referrer.indexOf(window.location.hostname) !== -1) {
                    window.location = document.referrer;
                }
            });
        },
        profile: function () {
            R.editor.password();
            R.editor.fileSelector();
        },
        register: function () {
            R.editor.password();
        },
        changePassword: function () {
            R.editor.password();
        }
    };

})(window.R = window.R || {});
